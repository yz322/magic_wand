const Speech = require('@google-cloud/speech');
const projectId = 'process.env.photon-155821';
const speechClient = Speech({
    projectId: projectId,
    credentials: require('./resources/keyfile1.json')
});
const fileName = './resources/test.wav';
const options = {
    encoding: 'LINEAR16',
    sampleRate: 8000,
    speechContext: {
        phrases: ["lumos", "nox", "turn on the tv", "turn off the tv"]
    }
};

const net = require('net');
const settings = {
  ip: "10.190.78.136",
  port: 3443
};

const fs = require("fs");

var Particle = require('particle-api-js');
var deviceID = '2c0032001047343432313031';
var accessToken = '33bda8307827ed7bd1b0baf79f57f8572a701cbf';
var particle = new Particle();
var transcription;
var chunkNum;

function startClient(){
	const client = net.connect(settings.port, settings.ip, function () {
	  	const outStream = fs.createWriteStream(fileName);
		chunkNum = 0;
		client.on("data", function (data) {
		    try {
		      outStream.write(data);
		      chunkNum++;
		    }
		    catch (ex) {
		        console.error("Er!" + ex);
		    }
		});
		client.on("end", function(){
			outStream.end();
			getTranscription();
			console.log(`contained ${chunkNum} chunks`);
			startClient();
		});
		client.on("error", function(){
			startClient();
		})
	});
}
startClient();
function castSpell(spell){
	console.log(`casting ${spell}`);
	if(spell == 'lumos'){
		lumos();
	}
	if(spell == 'nox'){
		nox();
	}
	if(spell.toLowerCase().includes('tv') && spell.toLowerCase().includes('on')){
	    turnOnTheTv();
    }
    if(spell.toLowerCase().includes('tv') && spell.toLowerCase().includes('off')){
	    turnOffTheTv();
    }
}
function getTranscription(){
	console.log('transcribing');
	speechClient.recognize(fileName, options)
	  .then((results) => {
	    transcription = results[0];
	    if (options.speechContext.phrases.includes(transcription.toLowerCase())){
		  	console.log(`about to cast: ${transcription}`);
	    	castSpell(transcription);	    	
	    }
	    else {
	    	console.log('Spell not recognized: ${transcription}');
	    }
	  }, (err) => {
	  	console.log(`ERROR: ${err}`);
	  });
}
var WeMo = new require('wemo')
var wemoSwitch = new WeMo('10.190.72.45', 49153);
function lumos(){
	wemoSwitch.setBinaryState(1, function(err, result) { 
	    if (err) console.error(err);
	});
}
function nox(){
	wemoSwitch.setBinaryState(0, function(err, result) { 
	    if (err) console.error(err);
	});
}
function turnOnTheTv() {
    particle.callFunction({ deviceId: deviceID, name: 'TVON', argument: 'KEY', auth: accessToken}).then(
        function (data) {
            console.log('Function called successfully tvOn: ', data.body.return_value);
        }, function (err) {
            console.log('An error occurred:', err);
        });
}
function turnOffTheTv() {
    particle.callFunction({ deviceId: deviceID, name: 'TVOFF', argument: 'KEY', auth: accessToken}).then(
        function (data) {
            console.log('Function called successfully tvOff: ', data.body.return_value);
        }, function (err) {
            console.log('An error occurred:', err);
        });
}

