#include <IRremote.h>

IRsend irsend;

void setup()
{
  Serial.begin(9600);
  pinMode(12,INPUT);
  pinMode(13,OUTPUT);
}

void loop() {
  String command = "";
  command = Serial.readStringUntil('X');
  if ((command == "TVON") || (command == "TVOFF"))
    sendCommand();
  delay(100); 
}

void sendCommand(){
  for (int i = 0; i < 3; i++) {
    irsend.sendSony(0xA90, 12);
    digitalWrite(13,HIGH);
    delay(40);
    digitalWrite(13,LOW);
  }
}

